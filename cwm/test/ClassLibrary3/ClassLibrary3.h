// ClassLibrary3.h

#pragma once

using namespace System;

namespace ClassLibrary3 {
	
	public ref class Class1
	{
	public:
		 //static int test(/*void *q,int i*/);
		 static int uq(void *q,int i);
		 static int uq2(void *q,int i);
		 static void uw(void *q,int i);
		 static void *uqq(int i);
		 static IntPtr uuqq(IntPtr q,int i);
		 static String ^ty(String ^q);
		 static int uuq(IntPtr q,int i);
		 static void nqq(void *q);
		 static void *wwq(void *q);
	//public :void getc(IntPtr c);
		 int yyy;
		// TODO: Add your methods for this class here.
	};
}
