﻿using System;
using ClassLibrary3;
namespace ConsoleApplication2
{
    class _void
    {
    }
    class _qq_list
    {
        public int n;
        public _qq_list() { n = 0; next = null; prev = null; }
        public qq _class;
        public _qq_list next, prev;
        public qq Find(IntPtr i)
        {
            if (n != 0)
            {
                if (ClassLibrary3.Class1._equal(i, _class._cs_class))
                    return _class;
                else
                    return next.Find(i);
            }
            else
            {
                qq newqq = new qq(new _void());
                _class = newqq;
                _class._cs_class = i;
                next = new _qq_list();
                next.prev = this;
                n = 1;
                return newqq;
            }
        }
        public void Del(IntPtr i)
        {
            if (n != 0)
            {
                if (ClassLibrary3.Class1._equal(i, _class._cs_class))
                {
                    next.prev = prev;
                    prev.next = next;
                }
                else
                    next.Del(i);
            }
            else
            {
                Console.WriteLine("не надо так");
            }
        }
        public void Add(qq i)
        {
            if (n != 0)
            {
                next.Add(i);
            }
            else
            {
                _class = i;
                next = new _qq_list();
                next.prev = this;
                n = 1;
            }
        }
    }
    class _ww_list
    {
        public int n;
        public _ww_list() { n = 0; next = null; prev = null; }
        public ww _class;
        public _ww_list next, prev;
        public ww Find(IntPtr i)
        {
            if (n != 0)
            {
                if (ClassLibrary3.Class1._equal(i, _class._cs_class))
                    return _class;
                else
                    return next.Find(i);
            }
            else
            {
                ww newww = new ww(new _void());
                _class = newww;
                _class._cs_class = i;
                next = new _ww_list();
                next.prev = this;
                n = 1;
                return newww;
            }
        }
        public void Del(IntPtr i)
        {
            if (n != 0)
            {
                if (ClassLibrary3.Class1._equal(i, _class._cs_class))
                {
                    next.prev = prev;
                    prev.next = next;
                }
                else
                    next.Del(i);
            }
            else
            {
                Console.WriteLine("не надо так");
            }
        }
        public void Add(ww i)
        {
            if (n != 0)
            {
                next.Add(i);
            }
            else
            {
                _class = i;
                next = new _ww_list();
                next.prev = this;
                n = 1;
            }
        }
    }
    static class _envy
    {
        public static _qq_list _qqlist = new _qq_list();
        public static _ww_list _wwlist = new _ww_list();
    }
    class qq
    {
        public IntPtr _cs_class = new IntPtr(0);
        public int q(int i)
        {
            return ClassLibrary3.Class1._c_qq_q(_cs_class, i);
        }
        public void w(int i)
        {
            ClassLibrary3.Class1._c_qq_w(_cs_class, i);
        }
        public String s(ww q)
        {
            return ClassLibrary3.Class1._c_qq_s(_cs_class, q._cs_class);
        }
        public String s()
        {
            return ClassLibrary3.Class1._c_qq_s(_cs_class);
        }
        public qq()
        {
            _cs_class = ClassLibrary3.Class1._c_qq_c_qq(_cs_class);
            _envy._qqlist.Add(this);
        }
        public void pr()
        {
            ClassLibrary3.Class1._c_qq_pr(_cs_class);
        }
        public int det()
        {
            return ClassLibrary3.Class1._c_qq_det(_cs_class);
        }
        public qq(int j)
        {
            _cs_class = ClassLibrary3.Class1._c_qq_c_qq(_cs_class, j);
            _envy._qqlist.Add(this);
        }
        public qq(String j)
        {
            _cs_class = ClassLibrary3.Class1._c_qq_c_qq(_cs_class, j);
            _envy._qqlist.Add(this);
        }
        public ww retg()
        {
            IntPtr _new_cs_class = new IntPtr();
            _new_cs_class = ClassLibrary3.Class1._c_qq_retg(_cs_class);
            return _envy._wwlist.Find(_new_cs_class);
        }
        ~qq()
        {
            ClassLibrary3.Class1._c_qq_d_qq(_cs_class);
        }
        public qq(_void __void)
        {
        }
    }
    class ww
    {
        public IntPtr _cs_class = new IntPtr(0);
        public qq q()
        {
            IntPtr _new_cs_class = new IntPtr();
            _new_cs_class = ClassLibrary3.Class1._c_ww_q(_cs_class);
            return _envy._qqlist.Find(_new_cs_class);
        }
        public int yyy()
        {
            return ClassLibrary3.Class1._c_ww_yyy(_cs_class);
        }
        public ww()
        {
            _cs_class = ClassLibrary3.Class1._c_ww_c_ww(_cs_class);
            _envy._wwlist.Add(this);
        }
        ~ww()
        {
            ClassLibrary3.Class1._c_ww_d_ww(_cs_class);
        }
        public ww(_void __void)
        {
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            qq matrix = new qq();
            matrix.pr();
            Console.WriteLine(matrix.det());
            Class1.rusinit();
            qq nya = new qq("ня?");
            Console.WriteLine(nya.s());
            ww nyaa = new ww();
            Console.WriteLine(nya.s(nyaa));
            _envy._qqlist._class = nya;
            _envy._qqlist.next = new _qq_list();
            ww tt = nya.retg();
            Console.Read();
        }
    }
}
