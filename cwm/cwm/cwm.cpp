// cwm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>
const int MAXSTRING=200;
#define CLASS "Class1"
#define CLASSLIB "ClassLibrary3"
#define _c_class "_c_class"
#define _cs_class "_cs_class"
#define _void "_void"
//char *destroy()
int kata=0;
const char qstr[]="qwertygreatandscary";
const char qc[]="q";
const char qs[]="cl";
char *gtp(char *q)
{
	while(!strchr(",.:;(){}<>[]\n*&",q[0])&&(q[0]!=0))q++;
	return q;
}
char *gte(char *q)
{
	while(!strchr(" ,.:;(){}<>[]\n*&",q[0])&&(q[0]!=0))q++;
	return q;
}
char *gtt(char *q)
{
	while(strchr(" ,.:;(){}<>[]\n*&",q[0])&&(q[0]!=0))q++;
	return q;
}
char *findstart(FILE *f,char *start,char *buf)
{
	while((start[0]!=';')&&(start[0]!='{')&&(start[0]!=0))start++;
	if(start[0]==0)
		while(fgets(buf,MAXSTRING,f))
		{
			while((buf[0]!=';')&&(buf[0]!='{')&&(buf[0]!=0))buf++;
			if(buf[0]!=0)return buf;
		}
}
int countb(char *q)
{
	int n=0;
	while(q[0]!=0)
	{
		if(q[0]=='{')n++;
		else
			if(q[0]=='}')n--;
		q++;
	}
	return n;
}
void importconfig(FILE *f,char *s)
{
	char str[MAXSTRING];
	FILE *q=fopen(s,"r");
	while(fgets(str,MAXSTRING,q))
		fwrite(str,strlen(str),1,f);
	fclose(q);
}
class Qclass;
class Qfunc;
class Qparam
{
public:
	char cdparam[100];
	char ccparam[100];
	char csdparam[100];
	char cscparam[100];
	int sign;
	bool isclass;
	bool isconst;
	bool isstring;
	char name[MAXSTRING];
	char type[MAXSTRING];
	int pointer;
	int ref;
	int length;
	Qparam *prev;//&
	Qparam *next;//&
	Qparam* Add(Qparam *n);//&
	Qparam(){prev=0;next=0;}
	Qparam(char *param);
	char *Init(char *s,Qfunc *own);
	void Createparam();
	int Tocsd(char *s);
	int Tocsc(char *s);
	int Tocd(char *s);
	int Tocc(char *s);
	//char &test(int **&& a){return 'j';};
};
int Qparam::Tocsd(char *s)
{
	char *q=s;
	if(isclass)
	{
		if((pointer<=1)&&(ref==0))
			q=&q[sprintf(q,"%s ",type)];
		else
			q=&q[sprintf(q,"IntPtr ")];
	}
	else
	{
		if(!pointer)
		{
			if(sign<0)
				if(!strcmp(type,"int")||!strcmp(type,"long")||!strcmp(type,"short"))
				{
					q=&q[sprintf(q,"u")];
				}
			q=&q[sprintf(q,"%s ",type)];
		}
		else 
		if(isstring)
			q=&q[sprintf(q,"String ")];
		else
			q=&q[sprintf(q,"IntPtr ")];
	}
	return (q-s);
}
int Qparam::Tocsc(char *s)
{
	char *q=s;
	if(isclass)
	{
		q=&q[sprintf(q,"IntPtr ")];
	}
	else
	{
		if(!pointer)
		{
			q=&q[sprintf(q,"%s ",type)];
		}
		else
		if(isstring)
			q=&q[sprintf(q,"String ")];
		else
			q=&q[sprintf(q,"IntPtr ")];
	}
	return (q-s);
}
int Qparam::Tocd(char *s)
{
	char *q=s;
	if(isclass)
	{
		q=&q[sprintf(q,"IntPtr ")];
	}
	else
	{
		if(!pointer)
		{
			q=&q[sprintf(q,"%s ",type)];
		}
		else
		if(isstring)
			q=&q[sprintf(q,"String ^")];
		else
			q=&q[sprintf(q,"IntPtr ")];
	}
	return (q-s);
}
int Qparam::Tocc(char *s)
{
	int p=pointer,r=ref;
	char *q=s;
	if(isconst)
		q=&q[sprintf(q,"const ")];
	if(sign<0)
		q=&q[sprintf(q,"unsigned ")];
	q=&q[sprintf(q,"%s ",type)];
	while(p)	
		{q=&q[sprintf(q,"*")];p--;}
	while(r)	
		{q=&q[sprintf(q,"&")];r--;}
	return (q-s);
}
void Qparam::Createparam()
{
	char *cdq=cdparam;
	char *ccq=ccparam;
	char *csdq=csdparam;
	char *cscq=cscparam;
	if(isconst)
	{
		cdq=&cdq[sprintf(cdq,"const ")];
		ccq=&ccq[sprintf(ccq,"const ")];
		csdq=&csdq[sprintf(csdq,"const ")];
		cscq=&cscq[sprintf(cscq,"const ")];
	}
	//cq=&cq[sprintf(cq,"%s ",type)];
	csdq=&csdq[Tocsd(csdq)];
	cscq=&csdq[Tocsc(cscq)];
	cdq=&cdq[Tocd(cdq)];
	ccq=&ccq[Tocc(ccq)];

	/*for(int i=0;i<pointer;i++)
		cq=&cq[sprintf(cq,"*")];
	for(int i=0;i<ref;i++)
		cq=&cq[sprintf(cq,"&")];*/
	/*cq=&cq[sprintf(cq,"%s",name)];
	csdq=&csdq[sprintf(csdq,"%s",name)];
	cscq=&cscq[sprintf(cscq,"%s",name)];*/
}
char *Qparam::Init(char *s,Qfunc *own)
{
	isconst=false;
	isstring=false;
	isclass=false;
	ref=0;
	sign=0;
	pointer=0;
	length=0;
	char c;
	char *q=s;//finish
	char *w=s;//start
	char *p=gtp(s);//start
	while ((p[0]=='*')||(p[0]=='&'))p=gtp(p+1);
params:
	if(gtt(gte(w))<p)
	{
		q=gte(w);
		c=q[0];
		q[0]=0;
		if(!strcmp(w,"const"))
		{
			isconst=true;
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		if(!strcmp(w,"signed"))
		{
			sign=1;
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		if(!strcmp(w,"unsigned"))
		{
			sign=-1;
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		if(!strcmp(w,"short"))
		{
			length--;
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		if(!strcmp(w,"long"))
		{
			length++;
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		if(!strcmp(w,"int"))
		{
			q[0]=c;
			w=gtt(q);
			goto params;
		}
		else
		{
			q[0]=c;
			p=s;
			goto params;
		}
	}
	else
	{
		//q++;
		//w=gtt(q);
		//q=gte(w);
		//q[0]=0;
		if(p!=s)//if int long short
		{
			if(length<0)
				strcpy(type,"short");
			else
			if(length==0)
				strcpy(type,"int");
			else
			if(length>0)
				strcpy(type,"long");
		}
		else
		{
			q=gte(w);
			c=q[0];
			q[0]=0;
			strcpy(type,w);
			q[0]=c;
			w=gtt(q);
			while((q=gtp(q))<w)
			{
				if(q[0]=='*')pointer++;
				if(q[0]=='&')ref++;
				q++;
			}
		}
		q=gte(w);
		c=q[0];
		q[0]=0;
		strcpy(name,w);
		q[0]=c;
		if((!strcmp(type,"char")||!strcmp(type,"__wchar_t")||!strcmp(type,"wchar_t"))&&(pointer==1))isstring=true;
		//Createparam();
		return q;
		
		//()
	}
	return s;
}
class Qfunc
{
public:	
	char cdparams[MAXSTRING];
	char cparams[MAXSTRING];
	char csdparams[MAXSTRING];
	char cscparams[MAXSTRING];
	bool isvirtual,isinline;
	int constr;
	int np;
	Qclass *owner;
	char name[MAXSTRING];
	Qparam *type;
	Qparam *param[MAXSTRING];
	void Init(char *s,Qclass *own);
	//void Createparams();
	void Makehfun(FILE *f); 
	void Makecppfun(FILE *f); 
	void Makecsfun(FILE *f); 
};
class Qclass
{
public:
	int nf;
	bool haveemptyconstr;
	bool haveemptydestr;
	Qclass *owner;
	char dllname[MAXSTRING];
	char name[MAXSTRING];
	char ownername[MAXSTRING];
	Qfunc *func[MAXSTRING];
	char *Init(FILE *f,char *start);
	char *ClassInit(char *start);
	void Makememstorage(FILE *f);
};
void Qfunc::Init(char *s,Qclass *own)
{
	type=0;
	char c;
	np=0;
	isvirtual=false;
	isinline=false;
	owner=own;
	char *q=s;//(),
	char *w=gte(s);//next text
	char *e=s;//this text
	char *r=s;//last text
	while (q[0]!='(')q++;
	if((w=gtt(w))>q)
	{
		if(s[0]=='~')
		{
			constr=-1;
			type=new(Qparam);
			type->isclass=false;
			type->isconst=false;
			type->isstring=false;
			type->length=0;
			sprintf(type->name,"");
			sprintf(type->type,"void");
			type->pointer=0;
			type->ref=0;
			type->sign=0;
		}
		else
		{
			constr=1;
			type=new(Qparam);
			type->isclass=true;
			type->isconst=false;
			type->isstring=false;
			type->length=0;
			sprintf(type->name,"%s",owner->name);
			sprintf(type->type,"%s",owner->name);
			type->pointer=1;
			type->ref=0;
			type->sign=0;
			type->Createparam();
		}
		gte(e)[0]=0;
		if(!constr)
		strcpy(name,e);
		else
			strcpy(name,owner->name);
	}
	else
	{
		w=s;
		constr=0;
		while(w<q)
		{
			//r=e;
			e=w;
			w=gte(w);
			c=w[0];
			w[0]=0;
			if(!strcmp(e,"virtual"))
				isvirtual=true;
			else
				if(!strcmp(e,"inline"))
					isinline =true;
				else
				{
					w[0]=c;
					type=new(Qparam);
					w=type->Init(e,this);
					//w=gtt(w);
					break;
				}
			w[0]=c;
			w=gtt(w);
		}
		/*gte(e)[0]=0;
		strcpy(name,e);
		e=s;
		w=s;*/
		//while()
	}
	while((w=gtt(w))[0])
		{
			param[np]=new(Qparam);
			w=param[np]->Init(w,this);
			np++;
		}
	if((constr==1))
		owner->haveemptyconstr=true;
	if((constr==-1))
		owner->haveemptydestr=true;
	//Createparams();

}
void Qclass::Makememstorage(FILE *f)
{
	char str[100];
	char *q=str;
	q=&q[sprintf(q,"\tclass _%s_list\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic int n;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic _%s_list() {n=0;next=null;prev=null;} \n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic %s _class;\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic _%s_list next,prev;\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic %s Find(IntPtr i)\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\tif(n!=0)\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tif(%s.%s._equal(i,_class.%s))\n",CLASSLIB,CLASS,_cs_class)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t\treturn _class;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\telse\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\treturn next.Find(i);\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\telse\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t%s new%s=new %s(new _void());\n",name,name,name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t_class=new%s;\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t_class.%s=i;\n",_cs_class,name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tnext=new _%s_list();\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tnext.prev=this;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tn=1;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\treturn new%s;\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic void Del(IntPtr i)\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\tif(n!=0)\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tif(%s.%s._equal(i,_class.%s))\n",CLASSLIB,CLASS,_cs_class)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t\t next.prev=prev;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t\tprev.next=next;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\telse\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t\tnext.Del(i);\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\telse\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\tConsole.WriteLine(\"�� ���� ���\");\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\tpublic void Add(%s i)\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\tif(n!=0)\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tnext.Add(i);\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\telse\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\t_class=i;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tnext=new _%s_list();\n",name)];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tnext.prev=this;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t\tn=1;\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t}\n")];
	fwrite(str,q-str,1,f);
	q=str;
}
void Qfunc::Makehfun(FILE *f)
{
	char str[100];
	char *q=str;
	q=&q[sprintf(q,"\t\tstatic ")];
	if(constr)
	{
		if(constr==1)
			q=&q[sprintf(q,"IntPtr _c_%s_c_%s(",owner->name,name)];
		if(constr==-1)
			q=&q[sprintf(q,"void _c_%s_d_%s(",owner->name,name)];
	}
	else
	q=&q[sprintf(q,"%s _c_%s_%s(",type->cdparam,owner->name,type->name)];
	q=&q[sprintf(q,"IntPtr %s",_c_class)];
	for(int i=0;i<np;i++)
		q=&q[sprintf(q,",%s%s",param[i]->cdparam,param[i]->name)];
	q=&q[sprintf(q,");\n")];
	fwrite(str,q-str,1,f);
}
void Qfunc::Makecppfun(FILE *f)
{
	int p=1;
	char str[100];
	char *q=str;
	q=&q[sprintf(q,"\t")];
	if(constr)
	{
		if(constr==1)
			q=&q[sprintf(q,"IntPtr %s::_c_%s_c_%s(",CLASS,owner->name,name)];
		if(constr==-1)
			q=&q[sprintf(q,"void %s::_c_%s_d_%s(",CLASS,owner->name,name)];
	}
	else
	q=&q[sprintf(q,"%s %s::_c_%s_%s(",type->cdparam,CLASS,owner->name,type->name)];
	q=&q[sprintf(q,"IntPtr %s",_c_class)];
	for(int i=0;i<np;i++)
		q=&q[sprintf(q,",%s%s",param[i]->cdparam,param[i]->name)];
	q=&q[sprintf(q,")\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t")];
	if(constr==-1)
		q=&q[sprintf(q,"delete((%s *)((void *)_c_class))",owner->name)];
	else
	{
		if(strcmp(type->type,"void")||(type->pointer!=0))
			q=&q[sprintf(q,"return ")];
		if(type->isstring)
		{
			q=&q[sprintf(q,"gcnew String(")];
			p++;
		}
		else
		if(!strcmp(type->cdparam,"IntPtr "))
		{
			q=&q[sprintf(q,"(IntPtr)")];
		}

		if(constr==1)
		{
			q=&q[sprintf(q,"new(%s)(",owner->name)];
		}
		else
		{
			q=&q[sprintf(q,"((%s *)((void *)(_c_class)))->%s(",owner->name,type->name)];
		}
		if(np)
		{
			if(!strcmp(param[0]->cdparam,"IntPtr "))
				q=&q[sprintf(q,"((%s)((void *)(%s)))",param[0]->ccparam,param[0]->name)];
			else
			if(param[0]->isstring)
			{
				q=&q[sprintf(q,"to%s(%s)",param[0]->type,param[0]->name)];
			}
			else
				q=&q[sprintf(q,"%s",param[0]->name)];
		}
		for(int i=1;i<np;i++)
		{
			if(!strcmp(param[i]->cdparam,"IntPtr "))
				q=&q[sprintf(q,",((%s)((void *)(%s)))",param[i]->ccparam,param[i]->name)];
			else
			if(param[i]->isstring)
			{
				q=&q[sprintf(q,",to%s(%s)",param[i]->type,param[i]->name)];
			}
			else
				q=&q[sprintf(q,",%s",param[i]->name)];
		}
		for(int i=0;i<p;i++)
			q=&q[sprintf(q,")")];
	}
		q=&q[sprintf(q,";\n")];
		fwrite(str,q-str,1,f);
		q=str;
		q=&q[sprintf(q,"\t}\n")];
		fwrite(str,q-str,1,f);



}
void Qfunc::Makecsfun(FILE *f)
{
	int p=1;
	char str[100];
	char *q=str;
	q=&q[sprintf(q,"\t\t")];
	if(constr)
	{
		if(constr==1)
			q=&q[sprintf(q,"public %s(",owner->name)];
		if(constr==-1)
			q=&q[sprintf(q,"~%s(",owner->name)];
	}
	else
		q=&q[sprintf(q,"public %s%s(",type->csdparam,type->name)];
	if(np)
	q=&q[sprintf(q,"%s%s",param[0]->csdparam,param[0]->name)];
	for(int i=1;i<np;i++)
		q=&q[sprintf(q,",%s%s",param[i]->csdparam,param[i]->name)];
	q=&q[sprintf(q,")\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t{\n")];
	fwrite(str,q-str,1,f);
	q=str;
	q=&q[sprintf(q,"\t\t\t")];
	if(constr==-1)
		q=&q[sprintf(q,"%s.%s._c_%s_d_%s(%s)",CLASSLIB,CLASS,owner->name,owner->name,_cs_class)];
	else
	{
		if(constr==1)
		{
			q=&q[sprintf(q,"%s=%s.%s._c_%s_c_%s(",_cs_class,CLASSLIB,CLASS,owner->name,owner->name)];
		}
		else
		if(type->isclass)
		{
			////there is a plase to choose method
			q=&q[sprintf(q,"IntPtr _new%s=new IntPtr();\n",_cs_class)];
			fwrite(str,q-str,1,f);
			q=str;
			q=&q[sprintf(q,"\t\t\t_new%s=",_cs_class)];
			q=&q[sprintf(q,"%s.%s._c_%s_%s(",CLASSLIB,CLASS,owner->name,type->name)];
		}
		else
		{
		if(strcmp(type->type,"void")||(type->pointer!=0))
			q=&q[sprintf(q,"return ")];
		if(type->sign<0)
		{
			q=&q[sprintf(q,"(u%s)",type->type)];
		}
		q=&q[sprintf(q,"%s.%s._c_%s_%s(",CLASSLIB,CLASS,owner->name,type->name)];
		}
		q=&q[sprintf(q,"%s",_cs_class)];
		for(int i=0;i<np;i++)
		{
			if((param[i]->isclass)&&(param[i]->pointer<2))
				q=&q[sprintf(q,",%s.%s",param[i]->name,_cs_class)];
			else
				q=&q[sprintf(q,",%s",param[i]->name)];
		}
		for(int i=0;i<p;i++)
			q=&q[sprintf(q,")")];
	}
	q=&q[sprintf(q,";\n")];
	fwrite(str,q-str,1,f);
	q=str;
	if((type->isclass)&&!constr)
	{
		q=&q[sprintf(q,"\t\t\treturn _envy._%slist.Find(_new%s);\n",type->type,_cs_class)];
		fwrite(str,q-str,1,f);
		q=str;
	}
	if(constr==1)
	{
		q=&q[sprintf(q,"\t\t\t_envy._%slist.Add(this);\n",name)];
		fwrite(str,q-str,1,f);
		q=str;
	}
	q=&q[sprintf(q,"\t\t}\n")];
	fwrite(str,q-str,1,f);
}
char *Qclass::ClassInit(char *start)
{
	nf=0;
	char *classname=start+6;
	char *q=gte(classname);
	char *w=gtp(q);
	if(w[0]=='\n')
	{
		q[0]=0;
		strcpy(name,classname);
		owner=0;
	}
	else
	{
		if(w[0]==':')
		{
			w=gtt(w);
			q[0]=0;
			strcpy(name,classname);
			classname=w;
			q=gte(classname);
			q[0]=0;
			strcpy(ownername,classname);
		}
	}



	return 0;
}
char *Qclass::Init(FILE *f,char *start)
{	
	haveemptyconstr=false;
	haveemptydestr=false;
	int balance=0;
	ClassInit(start);//parse classname
	char str[MAXSTRING];
	start=findstart(f,start,str);
		if(start==0)
			return 0;
		else
		if(start[0]==';')
			return 0;
	balance+=countb(start);//find {
	//while(balance==0){fgets(str,MAXSTRING,f);balance+=countb(str);};
	while(balance>0)
	{
		fgets(str,MAXSTRING,f);
		if(char *q=strstr(str,"dllexport"))
		{
			q=gte(q);
			q=gtt(q);
			func[nf]=new(Qfunc);
			func[nf]->Init(q,this);
			nf++;
		}
		balance+=countb(str);
	}
	return str;
}
Qparam* Qparam::Add(Qparam *n)
{
	prev=n;
	n->next=this;
	return n;
}
int main(int argc, char* argv[])
{
	
	char *errp=0;
	Qclass *classes[50];
	int nclasses=0;
	FILE *fh=fopen("Classlibrary3.h","w");//fopen(
	FILE *fcpp=fopen("Classlibrary3.cpp","w");//fopen(
	FILE *fcs=fopen("Program.cs","w");//fopen(
	//fwrite("#pragma once\nusing namespace System;\nnamespace ClassLibrary3 \n{\n\tpublic ref class Class1\n\t{\n\tpublic:",strlen("#pragma once\nusing namespace System;\nnamespace ClassLibrary3 \n{\n\tpublic ref class Class1\n\t{\n\tpublic:"),1,fh);
	//fwrite("#include \"stdafx.h\"\n#include \"ClassLibrary3.h\"\n#include \"..\\..\\test\\dll\\dll.h\"\nnamespace ClassLibrary3 \n{",strlen("#include \"stdafx.h\"\n#include \"ClassLibrary3.h\"\n#include \"..\\..\\test\\dll\\dll.h\"\nnamespace ClassLibrary3 \n{"),1,fcpp);
	char str[MAXSTRING];
	argc=3;
	FILE *f;
	for(int numh=1;numh<argc;numh++)
	{
		char str[MAXSTRING];
		if(numh==1)
		f=fopen(/*argv[numh]*/"dll.h","r");
		else
			f=fopen(/*argv[numh]*/"dll2.h","r");
		errp=&str[0];
		while(errp)
		{
			while(fgets(str,MAXSTRING,f)&&!(errp=strstr(str,"class ")));
			if(errp)
			{
gotoerrp:
				//if(kata)
					//return 0;
				classes[nclasses]=new(Qclass);
				errp=classes[nclasses]->Init(f,errp);
				nclasses++;
				if(errp==0)
				{
					nclasses--;
					delete(classes[nclasses]);
					errp=&str[0];
				}
				else
				{
					nclasses--;
					if(!classes[nclasses]->haveemptyconstr)
					{
						char newf[MAXSTRING];
						sprintf(newf,"%s ();",classes[nclasses]->name);
						classes[nclasses]->func[classes[nclasses]->nf]=new Qfunc();
						classes[nclasses]->func[classes[nclasses]->nf]->Init(newf,classes[nclasses]);
						classes[nclasses]->nf++;
					}
					if(!classes[nclasses]->haveemptydestr)
					{
						char newf[MAXSTRING];
						sprintf(newf,"~%s ();",classes[nclasses]->name);
						classes[nclasses]->func[classes[nclasses]->nf]=new Qfunc();
						classes[nclasses]->func[classes[nclasses]->nf]->Init(newf,classes[nclasses]);
						classes[nclasses]->nf++;
					}
					nclasses++;
					if(errp=strstr(errp,"class "))
						goto gotoerrp;
				}
			}
		}
		fclose(f);
	}
	for(int i1=0;i1<nclasses;i1++)
		for(int i2=0;i2<classes[i1]->nf;i2++)
		{
			if(classes[i1]->func[i2]->type)
				for(int i4=0;i4<nclasses;i4++)
				{
					if(!strcmp(classes[i1]->func[i2]->type->type,classes[i4]->name))
						classes[i1]->func[i2]->type->isclass=true;
					classes[i1]->func[i2]->type->Createparam();
				}
			for(int i3=0;i3<classes[i1]->func[i2]->np;i3++)
				for(int i4=0;i4<nclasses;i4++)
				{
					if(!strcmp(classes[i1]->func[i2]->param[i3]->type,classes[i4]->name))
						classes[i1]->func[i2]->param[i3]->isclass=true;
					classes[i1]->func[i2]->param[i3]->Createparam();
				}
		}
	
	//////h	
		
		
	sprintf(str,"#pragma once\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"using namespace System;\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"namespace ClassLibrary3 \n{\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"\tpublic ref class Class1\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"\t{\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"\tpublic:\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"\t\tstatic bool _equal(IntPtr _c_class,IntPtr i);\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"\t\tstatic void rusinit();\n");
	fwrite(str,strlen(str),1,fh);
	for(int i=0;i<nclasses;i++)
	{
		for(int a=0;a<classes[i]->nf;a++)
		{
			classes[i]->func[a]->Makehfun(fh);
		}
	}
	sprintf(str,"\t};\n");
	fwrite(str,strlen(str),1,fh);
	sprintf(str,"}\n");
	fwrite(str,strlen(str),1,fh);


	/////////cpp
	
	sprintf(str,"#include \"stdafx.h\"\n");
	fwrite(str,strlen(str),1,fcpp);
	sprintf(str,"#include \"ClassLibrary3.h\"\n");
	fwrite(str,strlen(str),1,fcpp);
	for(int i=1;i<argc;i++)
	{
		if(i==1)
			sprintf(str,"#include \"%s\"\n","..\\..\\test\\dll\\dll.h");
		else
			sprintf(str,"#include \"%s\"\n","..\\..\\test\\dll\\dll2.h");
		
		fwrite(str,strlen(str),1,fcpp);
	}
	sprintf(str,"namespace ClassLibrary3 \n");
	fwrite(str,strlen(str),1,fcpp);
	sprintf(str,"{\n");
	fwrite(str,strlen(str),1,fcpp);
	importconfig(fcpp,"cpp");
	/*sprintf(str,"\tbool  %s::_equal(IntPtr %s,IntPtr i)\n",CLASS,_c_class);
	fwrite(str,strlen(str),1,fcpp);
	sprintf(str,"\t{\n");
	fwrite(str,strlen(str),1,fcpp);
	sprintf(str,"\t\treturn (((void *)%s)==((void *)i));\n",_c_class);
	fwrite(str,strlen(str),1,fcpp);
	sprintf(str,"\t}\n");
	fwrite(str,strlen(str),1,fcpp);*/
	for(int i=0;i<nclasses;i++)
	{
		for(int a=0;a<classes[i]->nf;a++)
		{
			classes[i]->func[a]->Makecppfun(fcpp);
		}
	}
	sprintf(str,"}\n");
	fwrite(str,strlen(str),1,fcpp);

	///////cs
	sprintf(str,"using System;\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"using %s;\n",CLASSLIB);
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"namespace ConsoleApplication2\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"{\n");
	fwrite(str,strlen(str),1,fcs);
	importconfig(fcs,"cs");
	for(int i=0;i<nclasses;i++)
	{
		classes[i]->Makememstorage(fcs);
	}
	sprintf(str,"\tstatic class _envy\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t{\n");
	fwrite(str,strlen(str),1,fcs);
	for(int i=0;i<nclasses;i++)
	{
		sprintf(str,"\t public static _%s_list _%slist=new _%s_list();\n",classes[i]->name,classes[i]->name,classes[i]->name);
		fwrite(str,strlen(str),1,fcs);
	}
	sprintf(str,"\t}\n");
	fwrite(str,strlen(str),1,fcs);
	for(int i=0;i<nclasses;i++)
	{
		sprintf(str,"\tclass %s\n",classes[i]->name);
		fwrite(str,strlen(str),1,fcs);
		sprintf(str,"\t{\n",classes[i]->name);
		fwrite(str,strlen(str),1,fcs);
		sprintf(str,"\t\tpublic IntPtr %s=new IntPtr(0);\n",_cs_class);
		fwrite(str,strlen(str),1,fcs);
		for(int a=0;a<classes[i]->nf;a++)
		{
			classes[i]->func[a]->Makecsfun(fcs);
		}
		sprintf(str,"\t\tpublic %s(%s _%s)\n",classes[i]->name,_void,_void);
		fwrite(str,strlen(str),1,fcs);
		sprintf(str,"\t\t{\n");
		fwrite(str,strlen(str),1,fcs);
		sprintf(str,"\t\t}\n");
		fwrite(str,strlen(str),1,fcs);
		sprintf(str,"\t}\n");
		fwrite(str,strlen(str),1,fcs);
	}
	
	sprintf(str,"\tclass Program\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t{\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t\tstatic void Main(string[] args)\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t\t{\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t\t}\n");
	importconfig(fcs,"test");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"\t}\n");
	fwrite(str,strlen(str),1,fcs);
	sprintf(str,"}\n");
	fwrite(str,strlen(str),1,fcs);
	
	
	fclose(fcs);
	fclose(fh);
	fclose(fcpp);
	return 0;
}

