#include "stdafx.h"
#include "ClassLibrary3.h"
#include "..\..\test\dll\dll.h"
#include "..\..\test\dll\dll2.h"
namespace ClassLibrary3 
{
	char rus[2000];
	void  Class1::rusinit()
	{
		for(int i=0;i<128;i++)
			rus[i]=i;
		rus[1105]=-72;
		rus[1025]=-88;
		for(int i=0;i<32;i++)
			rus[1072+i]=-32+i;
		for(int i=0;i<32;i++)
			rus[1040+i]=-64+i;

	}

	char ruschar(int i)
	{
		return rus[i];
	}
	char *tochar(String ^s)
	{
		char *str=new(char [s->Length+1]);
		/*for(int i=0;i<256;i++)
		{
			str[i]=i;
		}*/
		int l=s->Length;
		for(int i=0;i<l;i++)
		{
			str[i]=ruschar(s[i]);
		}
		str[l]=0;
		return str;
	}
	wchar_t *towchar_t(String ^s)
	{
		wchar_t *str=new(wchar_t [s->Length+1]);
		int l=s->Length;
		for(int i=0;i<l;i++)
		{
			str[i]=s[i];
		}
		str[l]=0;
		return str;
	}
	__wchar_t *to__wchar_t(String ^s)
	{
		__wchar_t *str=new(__wchar_t [s->Length+1]);
		int l=s->Length;
		for(int i=0;i<l;i++)
		{
			str[i]=s[i];
		}
		str[l]=0;
		//wchar_t *ss=(wchar_t *)s->ToCharArray();
		return str;
	}	
	bool  Class1::_equal(IntPtr _c_class,IntPtr i)
	{
		return (((void *)_c_class)==((void *)i));
	}
	int  Class1::_c_qq_q(IntPtr _c_class,int i)
	{
		return ((qq *)((void *)(_c_class)))->q(i);
	}
	void  Class1::_c_qq_w(IntPtr _c_class,int i)
	{
		((qq *)((void *)(_c_class)))->w(i);
	}
	String ^ Class1::_c_qq_s(IntPtr _c_class,IntPtr q)
	{
		return gcnew String(((qq *)((void *)(_c_class)))->s(((ww *)((void *)(q)))));
	}
	String ^ Class1::_c_qq_s(IntPtr _c_class)
	{
		return gcnew String(((qq *)((void *)(_c_class)))->s());
	}
	IntPtr Class1::_c_qq_c_qq(IntPtr _c_class)
	{
		return (IntPtr)new(qq)();
	}
	void  Class1::_c_qq_pr(IntPtr _c_class)
	{
		((qq *)((void *)(_c_class)))->pr();
	}
	int  Class1::_c_qq_det(IntPtr _c_class)
	{
		return ((qq *)((void *)(_c_class)))->det();
	}
	IntPtr Class1::_c_qq_c_qq(IntPtr _c_class,int j)
	{
		return (IntPtr)new(qq)(j);
	}
	IntPtr Class1::_c_qq_c_qq(IntPtr _c_class,String ^j)
	{
		return (IntPtr)new(qq)(tochar(j));
	}
	IntPtr  Class1::_c_qq_retg(IntPtr _c_class)
	{
		return (IntPtr)((qq *)((void *)(_c_class)))->retg();
	}
	void Class1::_c_qq_d_qq(IntPtr _c_class)
	{
		delete((qq *)((void *)_c_class));
	}
	IntPtr  Class1::_c_ww_q(IntPtr _c_class)
	{
		return (IntPtr)((ww *)((void *)(_c_class)))->q();
	}
	int  Class1::_c_ww_yyy(IntPtr _c_class)
	{
		return ((ww *)((void *)(_c_class)))->yyy();
	}
	IntPtr Class1::_c_ww_c_ww(IntPtr _c_class)
	{
		return (IntPtr)new(ww)();
	}
	void Class1::_c_ww_d_ww(IntPtr _c_class)
	{
		delete((ww *)((void *)_c_class));
	}
}
